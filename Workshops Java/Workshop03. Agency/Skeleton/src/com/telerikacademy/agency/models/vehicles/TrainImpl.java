package com.telerikacademy.agency.models.vehicles;

import com.telerikacademy.agency.models.common.VehicleType;
import com.telerikacademy.agency.models.vehicles.contracts.Train;

public class TrainImpl extends VehicleBase implements Train {

    public static final int MIN_CARTS = 1;
    public static final int MAX_CARTS = 15;
    public static final int MIN_TRAIN_CAPACITY = 30;
    public static final int MAX_TRAIN_CAPACITY = 150;
    private int carts;

    public TrainImpl(int passengerCapacity, double pricePerKilometer, int carts) {
        super(passengerCapacity, pricePerKilometer, VehicleType.LAND);
        validatePassengerCapacity(passengerCapacity);
        setCarts(carts);
    }

    public void setCarts(int carts) {
        if(carts < MIN_CARTS || carts > MAX_CARTS){
            throw new IllegalArgumentException("A train cannot have less than 1 cart or more than 15 carts.");
        }
        this.carts = carts;
    }

    @Override
    public void validatePassengerCapacity(int passengerCapacity) {
        super.validatePassengerCapacity(passengerCapacity);
        if (passengerCapacity < MIN_TRAIN_CAPACITY || passengerCapacity > MAX_TRAIN_CAPACITY){
            throw new IllegalArgumentException("A train cannot have less than 30 passengers or more than 150 passengers.");
        }
    }

    @Override
    public int getPassengerCapacity() {
        return super.getPassengerCapacity();
    }

    @Override
    public double getPricePerKilometer() {
        return super.getPricePerKilometer();
    }

    @Override
    public VehicleType getType() {
        return super.getType();
    }

    @Override
    public int getCarts() {
        return carts;
    }

    @Override
    public String print() {
        return String.format("%s ----\n%sCarts amount:%s\n",printClassName(), super.print(), carts);
    }

    @Override
    public String toString() {
        return print();
    }
}
