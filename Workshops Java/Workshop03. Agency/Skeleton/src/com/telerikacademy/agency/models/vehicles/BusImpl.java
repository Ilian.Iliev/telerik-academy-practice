package com.telerikacademy.agency.models.vehicles;

import com.telerikacademy.agency.models.common.VehicleType;
import com.telerikacademy.agency.models.contracts.Printable;
import com.telerikacademy.agency.models.vehicles.contracts.Bus;

public class BusImpl extends VehicleBase implements Bus {

    public static final int MIN_BUS_CAPACITY = 10;
    public static final int MAX_BUS_CAPACITY = 50;

    public BusImpl(int passengerCapacity, double pricePerKilometer) {
        super(passengerCapacity, pricePerKilometer,VehicleType.LAND);
        validatePassengerCapacity(passengerCapacity);
    }

    @Override
    public void validatePassengerCapacity(int passengerCapacity) {
        super.validatePassengerCapacity(passengerCapacity);
        if (passengerCapacity < MIN_BUS_CAPACITY || passengerCapacity > MAX_BUS_CAPACITY){
            throw new IllegalArgumentException("A bus cannot have less than 10 passengers or more than 50 passengers.");
        }
    }

    @Override
    public String print() {
        return String.format("%s ----\n%s",printClassName(),super.print());
    }

    @Override
    public String toString() {
        return print();
    }
}
