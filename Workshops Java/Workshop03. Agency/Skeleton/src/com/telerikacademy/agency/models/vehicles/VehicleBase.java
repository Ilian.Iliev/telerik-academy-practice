package com.telerikacademy.agency.models.vehicles;

import com.telerikacademy.agency.models.common.VehicleType;
import com.telerikacademy.agency.models.contracts.Printable;
import com.telerikacademy.agency.models.vehicles.contracts.Vehicle;

public abstract class VehicleBase implements Vehicle {

    public static final int MIN_CAPACITY = 1;
    public static final int MAX_CAPACITY = 800;
    public static final double MIN_PRICE = 0.10;
    public static final double MAX_PRICE = 2.50;
    private int passengerCapacity;
    private double pricePerKilometer;
    private VehicleType type;
    
    public VehicleBase(int passengerCapacity, double pricePerKilometer,VehicleType type) {
        setPassengerCapacity(passengerCapacity);
        setPricePerKilometer(pricePerKilometer);
        if (type == null) {
            throw new IllegalArgumentException("TYPE_CANT_BE_NULL");
        }
        this.type = type;
    }

    public void setPassengerCapacity(int passengerCapacity) {
        validatePassengerCapacity(passengerCapacity);
        this.passengerCapacity = passengerCapacity;
    }


    public void setPricePerKilometer(double pricePerKilometer) {
        validatePricePerKilometer(pricePerKilometer);
        this.pricePerKilometer = pricePerKilometer;
    }


    public void validatePassengerCapacity (int passengerCapacity){
        if (passengerCapacity < MIN_CAPACITY || passengerCapacity > MAX_CAPACITY){
            throw new IllegalArgumentException("A vehicle with less than 1 passengers or more than 800 passengers cannot exist!");
        }
    }



    public void validatePricePerKilometer (double pricePerKilometer){
        if (pricePerKilometer < MIN_PRICE || pricePerKilometer > MAX_PRICE){
            throw new IllegalArgumentException("A vehicle with a price per kilometer lower than $0.10 or higher than $2.50 cannot exist!");
        }
    }
    @Override
    public int getPassengerCapacity() {
        return passengerCapacity;
    }

    @Override
    public double getPricePerKilometer() {
        return pricePerKilometer;
    }

    @Override
    public VehicleType getType() {
        return type;
    }

    @Override
    public String print() {
        return String.format("Passenger capacity: %s\nPrice per kilometer: %s\nVehicle type: %s\n",passengerCapacity,pricePerKilometer,type.toString());
    }

    public String printClassName() {
        return String.format("%s", getClass().getSimpleName().replace("Impl", ""));
    }

    @Override
    public String toString() {
        return print();
    }
}
