package com.telerikacademy.cosmetics.models.products;

import com.telerikacademy.cosmetics.models.common.GenderType;
import com.telerikacademy.cosmetics.models.common.ScentType;
import com.telerikacademy.cosmetics.models.contracts.Cream;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

public class CreamImpl extends ProductBase implements Cream {
    private ScentType scent;
    
    public CreamImpl(String name, String brand, double price, GenderType gender, ScentType scent) {
        super(name, brand, price, gender);
        this.scent = scent;
    }

    @Override
    public ScentType getScent() {
        return scent;
    }

    @Override
    public String print() {
        String prd = super.print();
        prd = prd + String.format(" #Scent: %s \n",getScent());
        return prd;
    }
}
