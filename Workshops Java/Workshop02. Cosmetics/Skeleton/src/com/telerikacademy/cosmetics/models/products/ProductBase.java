package com.telerikacademy.cosmetics.models.products;

import com.telerikacademy.cosmetics.models.common.GenderType;
import com.telerikacademy.cosmetics.models.contracts.Product;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

public class ProductBase implements Product {
    public static final int MIN_NAME_LENGHT = 3;
    public static final int MAX_NAME_LENGHT = 10;
    public static final int MIN_BRAND_NAME = 2;
    public static final int MAX_BRAND_NAME = 10;
    //Finish the class
    //implement proper interface (see contracts package)
    //validate
    private String name;
    private String brand;
    private double price;
    private GenderType gender;

    
    ProductBase(String name, String brand, double price, GenderType gender) {
        //throw new NotImplementedException();
        if (name == null){
            throw new IllegalArgumentException("Name can not be null!");
        }
        if (brand == null){
            throw new IllegalArgumentException("Brand name can not be null!");
        }
        if(name.length() < MIN_NAME_LENGHT || name.length() > MAX_NAME_LENGHT){
            throw new IllegalArgumentException("Name must contains between 3 and 10 symbols!");
        }
        if (brand.length() < MIN_BRAND_NAME || brand.length() > MAX_BRAND_NAME){
            throw new IllegalArgumentException("Name must contains between 2 and 10 symbols!");
        }
        if (price < 0) {
            throw new IllegalArgumentException("the price is negative!");
        }
        setName(name);
        setGender(gender);
        setBrand(brand);
        setPrice(price);

    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setBrand(String brand) {

        this.brand = brand;
    }

    public String getBrand() {
        return brand;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public double getPrice() {
        return price;
    }

    public void setGender(GenderType gender) {
        this.gender = gender;
    }

    public GenderType getGender() {
        return gender;
    }

    @Override
    public String print() {
        return String.format("#%s %s \n #Price: %s \n #Gender: %s \n", getName(), getBrand(),getPrice(), getGender());
    }
}

