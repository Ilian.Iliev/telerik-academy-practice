package com.telerikacademy.cosmetics.models.products;

import com.telerikacademy.cosmetics.models.common.GenderType;
import com.telerikacademy.cosmetics.models.common.UsageType;
import com.telerikacademy.cosmetics.models.contracts.Shampoo;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

public class ShampooImpl extends ProductBase implements Shampoo {
    private int milliliters;
    private UsageType type;
    
    public ShampooImpl(String name, String brand, double price, GenderType gender, int milliliters, UsageType type) {
        super(name, brand, price, gender);
        this.milliliters = milliliters;
        this.type = type;
    }

    @Override
    public int getMilliliters() {
        return milliliters;
    }

    @Override
    public UsageType getUsage() {
        return type;
    }

    @Override
    public String print() {
        String prd = super.print();
        prd = prd + String.format(" #Milliliters: %s \n #Usage: %s \n",getMilliliters(), getUsage());
        return prd;
    }
}
