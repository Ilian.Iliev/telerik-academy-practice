package com.telerikacademy.cosmetics.models.products;

import com.telerikacademy.cosmetics.models.common.GenderType;
import com.telerikacademy.cosmetics.models.contracts.Toothpaste;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

import java.util.ArrayList;
import java.util.List;

public class ToothpasteImpl extends ProductBase implements Toothpaste {

    private List<String> ingredients;
    
    public ToothpasteImpl(String name, String brand, double price, GenderType gender, List<String> ingredientsArg) {
        super(name, brand, price, gender);
        if (ingredientsArg!=null){
            ingredients = ingredientsArg;
        }
        else {
            throw new IllegalArgumentException("Ingredients can not be null!");
        }
    }

    @Override
    public List<String> getIngredients() {
        return new ArrayList<>(ingredients);
    }

    @Override
    public String print() {
        String prd = super.print();
        prd = prd + " #Ingredients:";
        for (int i = 0; i < ingredients.size(); i++) {
            if (i == ingredients.size()-1 ) {
                prd = prd + ingredients.get(i);
            }
            else{
                prd = prd + ingredients.get(i) + " ,";
            }
        }
        return prd + "\n";
    }
}
