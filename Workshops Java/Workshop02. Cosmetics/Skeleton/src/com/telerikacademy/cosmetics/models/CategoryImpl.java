package com.telerikacademy.cosmetics.models;


import com.telerikacademy.cosmetics.models.contracts.Category;
import com.telerikacademy.cosmetics.models.contracts.Product;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

import java.util.ArrayList;
import java.util.List;

public class CategoryImpl implements Category {
    public static final int MIN_CAT_NAME = 2;
    public static final int MAX_CAT_NAME = 15;
    
    private String name;
    private List<Product> products;
    
    public CategoryImpl(String name) {
        if (name.length() < MIN_CAT_NAME || name.length() > MAX_CAT_NAME){
            throw new IllegalArgumentException("Name must contains between 2 adn 15 symbols!");
        }
        this.name = name;
        products = new ArrayList<>();
    }
    
    public String getName() {
        return name;
    }
    
    public List<Product> getProducts() {
        //todo why are we returning a copy? Replace this comment with explanation!
        //To protect the original list
        return new ArrayList<>(products);
    }
    
    public void addProduct(Product product) {
        if (product == null) {
            throw new IllegalArgumentException("Product must be not null!");
        }
        products.add(product);
    }
    
    public void removeProduct(Product product) {
        if (product == null){
            throw new IllegalArgumentException("Product must be not null!");
        }
        if (!products.remove(product)) {
            throw new IllegalArgumentException("Product not found!");
        }

    }

    public String print() {
        if (products.size() == 0) {
            return String.format("#Category: %s\n" +
                    " #No product in this category", name);
        }
        String categories = String.format("#Category: %s", name);
        for (Product product : products) {
            categories += "\n" + product.print();
        }
        return  categories;

    }
    
}
