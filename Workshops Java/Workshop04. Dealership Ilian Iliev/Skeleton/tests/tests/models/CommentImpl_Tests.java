package tests.models;

import com.telerikacademy.dealership.models.CommentImpl;
import com.telerikacademy.dealership.models.contracts.Comment;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class CommentImpl_Tests {
    
    @Test
    public void CommentImpl_ShouldImplementCommentInterface() {
        CommentImpl comment = new CommentImpl("content", "author");
        Assertions.assertTrue(comment instanceof Comment);
    }
    
    @Test
    public void Constructor_ShouldThrow_WhenContentIsNull() {
        Assertions.assertThrows(NullPointerException.class,
                () -> new CommentImpl(null, "author"));
    }
    
    @Test
    public void Constructor_ShouldThrow_WhenContentIsBelow3() {
        Assertions.assertThrows(IllegalArgumentException.class,
                () -> new CommentImpl("co", "author"));
    }
    
    @Test
    public void Constructor_ShouldThrow_WhenContentIsAbove200() {
        Assertions.assertThrows(IllegalArgumentException.class,
                () -> new CommentImpl(new String(new char[250]), "author"));
    }
    
}
