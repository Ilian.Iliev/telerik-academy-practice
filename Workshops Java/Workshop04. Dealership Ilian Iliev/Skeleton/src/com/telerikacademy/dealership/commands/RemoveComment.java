package com.telerikacademy.dealership.commands;

import com.telerikacademy.dealership.commands.contracts.Command;
import com.telerikacademy.dealership.core.contracts.DealershipFactory;
import com.telerikacademy.dealership.core.contracts.DealershipRepository;
import com.telerikacademy.dealership.models.common.Validator;
import com.telerikacademy.dealership.models.contracts.Comment;
import com.telerikacademy.dealership.models.contracts.User;
import com.telerikacademy.dealership.models.contracts.Vehicle;

import java.util.List;

import static com.telerikacademy.dealership.commands.constants.CommandConstants.*;

public class RemoveComment implements Command {
    
    private final DealershipFactory dealershipFactory;
    
    public RemoveComment(DealershipFactory dealershipFactory, DealershipRepository dealershipRepository) {
        this.dealershipFactory = dealershipFactory;
        this.dealershipRepository = dealershipRepository;
    }
    
    private final DealershipRepository dealershipRepository;
    
    
    @Override
    public String execute(List<String> parameters) {
        int vehicleIndex = Integer.parseInt(parameters.get(0)) - 1;
        int commentIndex = Integer.parseInt(parameters.get(1)) - 1;
        String username = parameters.get(2);
        
        return removeComment(vehicleIndex, commentIndex, username);
    }
    
    private String removeComment(int vehicleIndex, int commentIndex, String username) {
        User user = dealershipRepository.getUsers().stream().filter(u -> u.getUsername().toLowerCase().equals(username.toLowerCase())).findFirst().orElse(null);
        
        if (user == null) {
            return String.format(NO_SUCH_USER, username);
        }
        
        Validator.ValidateIntRange(vehicleIndex, 0, user.getVehicles().size(), REMOVED_VEHICLE_DOES_NOT_EXIST);
        Validator.ValidateIntRange(commentIndex, 0, user.getVehicles().get(vehicleIndex).getComments().size(), REMOVED_COMMENT_DOES_NOT_EXIST);
        
        Vehicle vehicle = user.getVehicles().get(vehicleIndex);
        Comment comment = user.getVehicles().get(vehicleIndex).getComments().get(commentIndex);
        
        dealershipRepository.getLoggedUser().removeComment(comment, vehicle);
        
        return String.format(COMMENT_REMOVED_SUCCESSFULLY, dealershipRepository.getLoggedUser().getUsername());
    }
    
}
