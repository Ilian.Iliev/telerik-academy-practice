package com.telerikacademy.dealership.models;


import com.telerikacademy.dealership.models.common.enums.VehicleType;
import com.telerikacademy.dealership.models.contracts.Motorcycle;

public class MotorcycleImpl extends VehicleBase implements Motorcycle {

    private String category;

    public MotorcycleImpl(String make, String model, double price, String category) {
        super(make, model, price, VehicleType.MOTORCYCLE);
        setCategory(category);

    }

    public void setCategory(String category) {
        if (category.length() < ModelsConstants.MIN_CATEGORY_LENGTH || category.length() > ModelsConstants.MAX_CATEGORY_LENGTH) {
            throw new IllegalArgumentException("Category must be between 3 and 10 symbols!");
        }
        this.category = category;
    }

    @Override
    public String getCategory() {
        return category;
    }

    @Override
    public int getWheels() {
        return VehicleType.MOTORCYCLE.getWheelsFromType();
    }

    @Override
    public String toString() {
        return super.toString();
    }


    @Override
    protected String printAdditionalInfo() {
        return String.format("Category: %s\n", category);
    }
}
