package com.telerikacademy.dealership.models;

import com.telerikacademy.dealership.models.common.Utils;
import com.telerikacademy.dealership.models.common.enums.VehicleType;
import com.telerikacademy.dealership.models.contracts.Comment;
import com.telerikacademy.dealership.models.contracts.Vehicle;

import java.util.ArrayList;
import java.util.List;

public abstract class VehicleBase implements Vehicle {

    private final static String MAKE_FIELD = "Make";
    private final static String MODEL_FIELD = "Model";
    private final static String PRICE_FIELD = "Price";
    private final static String WHEELS_FIELD = "Wheels";
    private final static String COMMENTS_HEADER = "    --COMMENTS--";
    private final static String NO_COMMENTS_HEADER = "    --NO COMMENTS--";

    private List<Comment> comments = new ArrayList<>();
    private String make;
    private String model;
    private double price;
    private VehicleType vehicleType;

    public VehicleBase(String make, String model, double price, VehicleType vehicleType) {
        setMake(make);
        setModel(model);
        setPrice(price);
        setVehicleType(vehicleType);
    }

    private void setMake(String make) {
        if (make.length() < ModelsConstants.MIN_MAKE_LENGTH || make.length() > ModelsConstants.MAX_MAKE_LENGTH) {
            throw new IllegalArgumentException("Make must be between 2 and 15 characters long!");
        }
        this.make = make;
    }

    private void setModel(String model) {
        if (model.length() < ModelsConstants.MIN_MODEL_LENGTH || model.length() > ModelsConstants.MAX_MODEL_LENGTH) {
            throw new IllegalArgumentException("Model must be between 1 and 15 characters long!");
        }
        this.model = model;
    }

    private void setPrice(double price) {
        if (price < ModelsConstants.MIN_PRICE || price > ModelsConstants.MAX_PRICE) {
            throw new IllegalArgumentException("Invalid price!");
        }
        this.price = price;
    }

    private void setVehicleType(VehicleType vehicleType) {
        this.vehicleType = vehicleType;
    }

    @Override
    public List<Comment> getComments() {
        return new ArrayList<>(comments);
    }

    @Override
    public double getPrice() {
        return price;
    }

    @Override
    public VehicleType getType() {
        return vehicleType;
    }

    @Override
    public String getMake() {
        return make;
    }

    @Override
    public String getModel() {
        return model;
    }

    @Override
    public void removeComment(Comment comment) {
        comments.remove(comment);
    }

    @Override
    public void addComment(Comment comment) {
        comments.add(comment);
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();

        builder.append(String.format("%s:", this.getClass().getSimpleName().replace("Impl", ""))).append(System.lineSeparator());
        builder.append(String.format("%s: %s", MAKE_FIELD, make)).append(System.lineSeparator());
        builder.append(String.format("%s: %s", MODEL_FIELD, model)).append(System.lineSeparator());
        builder.append(String.format("%s: %s", WHEELS_FIELD, vehicleType.getWheelsFromType())).append(System.lineSeparator());

        builder.append(String.format("%s: $%s", PRICE_FIELD, Utils.removeTrailingZerosFromDouble(price))).append(System.lineSeparator());

        if (!printAdditionalInfo().isEmpty()) {
            builder.append(printAdditionalInfo()).append(System.lineSeparator());
        }
        builder.append(printComments());
        return builder.toString();
    }

    //todo replace this comment with explanation why this method is protected:
    //The method is used only in the child classes and in every class who extends VehicleBase.
    //We don't need acces to this method from anywhere else.
    protected abstract String printAdditionalInfo();

    private String printComments() {
        StringBuilder builder = new StringBuilder();

        if (comments.size() <= 0) {
            builder.append(String.format("%s", NO_COMMENTS_HEADER));
        } else {
            builder.append(String.format("%s", COMMENTS_HEADER)).append(System.lineSeparator());

            for (Comment comment : comments) {
                builder.append(comment.toString());
            }

            builder.append(String.format("%s", COMMENTS_HEADER));
        }

        return builder.toString();
    }

}
