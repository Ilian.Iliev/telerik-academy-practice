package com.telerikacademy.dealership.models;

import com.telerikacademy.dealership.models.common.enums.VehicleType;
import com.telerikacademy.dealership.models.contracts.Car;

public class CarImpl extends VehicleBase implements Car {

    private int seats;

    public CarImpl(String make, String model, double price, int seats) {
        super(make, model, price, VehicleType.CAR);
        setSeats(seats);
    }

    private void setSeats(int seats) {
        if (seats < ModelsConstants.MIN_SEATS || seats > ModelsConstants.MAX_SEATS) {
            throw new IllegalArgumentException("Seats must be between 1 adn 10!");
        }
        this.seats = seats;
    }

    @Override
    public int getSeats() {
        return seats;
    }

    @Override
    public int getWheels() {
        return VehicleType.CAR.getWheelsFromType();
    }

    @Override
    public String toString() {
        return super.toString();
    }

    @Override
    protected String printAdditionalInfo() {
        return String.format("Seats: %s\n", seats);
    }

}
