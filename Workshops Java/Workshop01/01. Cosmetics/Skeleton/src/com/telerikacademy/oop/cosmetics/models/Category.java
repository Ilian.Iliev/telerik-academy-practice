package com.telerikacademy.oop.cosmetics.models;

import com.telerikacademy.oop.cosmetics.models.products.Product;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Category {
    
    private String name;
    private List<Product> products = new ArrayList<>();
    
    public Category(String name) {
        setName(name);
    }

    public void setName(String name) {
        validateName(name);
        this.name = name;
    }

    public String getName() {
        return name;
    }

    private void validateName(String name) {
        if (name.length() < 2 || name.length() > 15){
            throw new IllegalArgumentException();
        }
    }

    public List<Product> getProducts() {
        return Collections.unmodifiableList(products);
    }
    
    public void addProduct(Product product) {
        if (product != null) {
            products.add(product);
        }
        else{
            throw new IllegalArgumentException("Product is null");
        }
    }
    
    public void removeProduct(Product product) {
        if (product == null){
            throw new IllegalArgumentException("Product must not be null");
        }
        if (!products.remove(product)) {
            throw new IllegalArgumentException("Product not found");
        }
    }
    
    public String print() {
        return constructPrint();
    }

    private String constructPrint() {
        StringBuilder returnStatement = new StringBuilder();
        returnStatement.append("#Category {").append(name).append("}\n");

        if (products.isEmpty()){
            returnStatement.append("#No product in this category");
            return returnStatement.toString();
        }

        for (Product product: products) {
            returnStatement.append(product.print()).append("\n");
        }
        return returnStatement.toString();
    }

}
