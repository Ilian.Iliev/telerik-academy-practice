package com.telerikacademy.oop.cosmetics.models.cart;

import com.telerikacademy.oop.cosmetics.models.products.Product;

import java.util.ArrayList;
import java.util.List;

public class ShoppingCart {
    
    private List<Product> productList = new ArrayList<>();
    
    public ShoppingCart() {
    }
    
    public List<Product> getProductList() {

        return new ArrayList<>(productList);
    }
    
    public void addProduct(Product product) {
        if (product != null) {
            productList.add(product);
        }
        else{
            throw new IllegalArgumentException();
        }
    }
    
    public void removeProduct(Product product) {
        if(product == null){
            throw new IllegalArgumentException();
        }
        productList.remove(product);
    }
    
    public boolean containsProduct(Product product) {
        if (product == null){
            throw new IllegalArgumentException();
        }

        return productList.contains(product);
    }
    
    public double totalPrice() {

        double total = 0;
        for (Product product:productList) {
            total += product.getPrice();
        }
        return total;
    }
    
}
