package com.telerikacademy.oop.cosmetics.models.products;

import com.telerikacademy.oop.cosmetics.models.common.GenderType;

public class Product {
    
    private double price;
    private String name;
    private String brand;
    private GenderType gender;
    
    public Product(String name, String brand, double price, GenderType gender) {
        setName(name);
        setPrice(price);
        setBrand(brand);
        setGender(gender);
        // finish the constructor and validate data
    //    setPrice(price);
    }

    public double getPrice() {

        return price;
    }

    public void setPrice(double price) {
        validatePrice(price);
        this.price = price;
    }

    public String getName() {

        return name;
    }

    public void setName(String name) {
        validateName(name);
        this.name = name;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        validateBrand(brand);
        this.brand = brand;
    }

    public GenderType getGender() {
        return gender;
    }

    public void setGender(GenderType gender) {

        this.gender = gender;
    }

    public String print() {

        return "#[" + name + "] [" + brand + "]\n" +
                "#Price: [" + price + "]\n" +
                "#Gender: [" + gender.toString() + "]\n===";
        // Format:
        //" #[Name] [Brand]
        // #Price: [Price]
        // #Gender: [Gender]
        // ==="
    }

    private void validateName(String name){
        if (name.length() < 3 || name.length() > 10){
            throw new IllegalArgumentException("Invalid name!");
        }
    }

    private void validateBrand(String brand){
        if(brand.length() < 2 || brand.length() > 10){
            throw new IllegalArgumentException("Invalid brand name!");
        }
    }

    private void validatePrice(double price){
        if(price <= 0){
            throw new IllegalArgumentException("Invalid price!");
        }
    }


    
}
