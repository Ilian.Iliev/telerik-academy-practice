import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class TestsTelerikacademyForum {

    private final String URL = "https://stage-forum.telerikacademy.com/";
    private final String EMAIL = "buggs.bunnies.telerik@gmail.com";
    private final String PASSWORD = "Buggs-Bunn1es";
    private final String CONTENT_FIELD = "//textarea[@aria-label='Type here. Use Markdown, BBCode, or HTML to format. Drag or paste images.']";
    private final String LOGIN_BUTTON = "//button[@class='widget-button btn btn-primary btn-small login-button btn-icon-text']";

    @Test
    public void openNewTopicWithTitleAndContent(){
        System.setProperty("webdriver.chrome.driver","C:\\Users\\PC\\Desktop\\drivers\\chromedriver.exe");
        WebDriver webdriver = new ChromeDriver();
        WebDriverWait wait = new WebDriverWait(webdriver,3);
        //Arrange
        webdriver.get(URL);

        //Act
        WebElement loginButton = webdriver.findElement(By.xpath(LOGIN_BUTTON));
        loginButton.click();

        WebElement emailField = webdriver.findElement(By.id("Email"));
        emailField.sendKeys(EMAIL);

        WebElement passwordField = webdriver.findElement(By.id("Password"));
        passwordField.sendKeys(PASSWORD);

        WebElement signInButton = webdriver.findElement(By.id("next"));
        signInButton.click();

        WebElement newTopic = webdriver.findElement(By.id("create-topic"));
        newTopic.click();

        WebElement titleField = webdriver.findElement(By.id("reply-title"));

        long min = 10000000000L;
        long max = 20000000000L;
        long randomTitle = (long)Math.floor(Math.random()*(max-min+1)+min);
        long randomContent = (long)Math.floor(Math.random()*(max-min+1)+min);

        titleField.sendKeys("Title " + randomTitle);

        WebElement contentField = webdriver.findElement(By.xpath(CONTENT_FIELD));
        contentField.sendKeys("Text " + randomContent);

        WebElement createTopicButton = webdriver.findElement(By.xpath("//button[@aria-label='Create Topic']"));
        wait.until(ExpectedConditions.visibilityOf(createTopicButton));
        createTopicButton.click();

        WebElement createdTopicForAssertion = webdriver.findElement(By.xpath("//a[@href='/u/buggs.bunnies.teleri']"));

        //Assert
        Assert.assertTrue("Created topic was not displayed.",createdTopicForAssertion.isDisplayed());

        //Logout
        WebElement userMenu = webdriver.findElement(By.id("current-user"));
        userMenu.click();
        WebElement preferences = webdriver.findElement(By.xpath("//button[@aria-controls='quick-access-profile']"));
        wait.until(ExpectedConditions.visibilityOf(preferences));
        preferences.click();
        WebElement logoutButton = webdriver.findElement(By.xpath("//span[text()='Log Out']"));
        wait.until(ExpectedConditions.visibilityOf(logoutButton));
        logoutButton.click();

        webdriver.close();
        webdriver.quit();
    }

    @Test
    public void tryToOpenNewTopicWithoutTitle(){
        System.setProperty("webdriver.chrome.driver","C:\\Users\\PC\\Desktop\\drivers\\chromedriver.exe");
        WebDriver webdriver = new ChromeDriver();
        WebDriverWait wait = new WebDriverWait(webdriver,6);
        //Arrange
        webdriver.get(URL);

        //Act
        WebElement loginButton = webdriver.findElement(By.xpath(LOGIN_BUTTON));
        loginButton.click();

        WebElement emailField = webdriver.findElement(By.id("Email"));
        emailField.sendKeys(EMAIL);

        WebElement passwordField = webdriver.findElement(By.id("Password"));
        passwordField.sendKeys(PASSWORD);

        WebElement signInButton = webdriver.findElement(By.id("next"));
        signInButton.click();

        WebElement newTopic = webdriver.findElement(By.id("create-topic"));
        newTopic.click();

        WebElement createTopicButton = webdriver.findElement(By.xpath("//button[@aria-label='Create Topic']"));
        wait.until(ExpectedConditions.visibilityOf(createTopicButton));
        createTopicButton.click();

        //Assert
        Assert.assertTrue("Created topic was not displayed.",createTopicButton.isDisplayed());

        //Logout
        WebElement userMenu = webdriver.findElement(By.id("current-user"));
        userMenu.click();
        WebElement preferences = webdriver.findElement(By.xpath("//button[@aria-controls='quick-access-profile']"));
        wait.until(ExpectedConditions.visibilityOf(preferences));
        preferences.click();
        WebElement logoutButton = webdriver.findElement(By.xpath("//span[text()='Log Out']"));
        wait.until(ExpectedConditions.visibilityOf(logoutButton));
        logoutButton.click();

        webdriver.close();
        webdriver.quit();
    }
}
