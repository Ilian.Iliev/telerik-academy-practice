package stepDefinitions;

import com.telerikacademy.testframework.UserActions;
import org.jbehave.core.annotations.AfterScenario;
import org.jbehave.core.annotations.AfterStory;
import org.jbehave.core.annotations.BeforeScenario;
import org.jbehave.core.annotations.BeforeStory;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import pages.telerik.forum.ForumHomePage;
import pages.telerik.forum.ForumLoginPage;
import pages.telerik.forum.ForumTopicsPage;

public class BaseStepDefinition {


    UserActions actions = new UserActions();
    ForumHomePage home = new ForumHomePage(actions.getDriver());
    ForumLoginPage login = new ForumLoginPage(actions.getDriver());
    ForumTopicsPage topics = new ForumTopicsPage(actions.getDriver());

    @BeforeClass
    public static void setUp(){
        UserActions.loadBrowser("forum.homePage");
    }

    @AfterClass
    public static void tearDown(){
        UserActions.quitDriver();
    }

    @AfterStory
    public void logOut() {
        topics.logOut();    
    }
}
