package stepDefinitions;

import org.jbehave.core.annotations.Given;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;

public class StepDefinitions extends BaseStepDefinition {

    @Given("Telerik academy forum homepage is opened")
    public void homePageIsVisible(){
        home.navigateToPage();
    }

    @When("Log in with valid credentials")
    public void logIn(){
        home.assertLogInButtonVisible();
        home.clickLogInButton();
        login.enterEmail();
        login.enterPassword();
        login.clickSignInButton();
    }

    @When("Login")
    public void autoLogin(){
        home.clickLogInButton();
    }

    @When("Create a new topic with valid title and content")
    public void newTopicCreation(){
        topics.clickNewTopicButton();
        topics.enterTitle();
        topics.enterContent();
        topics.clickCreateTopicButton();
    }

    @When("Create a new topic without title and content")
    public void newTopicWithoutTitleAndContent(){
        topics.clickNewTopicButton();
        topics.clickCreateTopicButton();
    }

    @Then("The new topic should be created")
    public void assertTopicIsCreated(){
        topics.assertionForCreatedTopic();
    }

    @Then("You should not be able to create a topic without title and content")
    public void assertTopicIsNotCreated(){
        topics.assertionForFailedCreationOfTopic();
    }
}
