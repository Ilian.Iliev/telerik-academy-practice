package tests.cases.forum;

import com.telerikacademy.testframework.UserActions;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import pages.telerik.forum.ForumHomePage;
import pages.telerik.forum.ForumLoginPage;
import pages.telerik.forum.ForumTopicsPage;

public class BaseTest {
	UserActions actions = new UserActions();
	ForumHomePage home = new ForumHomePage(actions.getDriver());
	ForumLoginPage login = new ForumLoginPage(actions.getDriver());
	ForumTopicsPage topics = new ForumTopicsPage(actions.getDriver());

	@BeforeClass
	public static void setUp(){
		UserActions.loadBrowser("forum.homePage");
	}

	@AfterClass
	public static void tearDown(){
		UserActions.quitDriver();
	}
}
