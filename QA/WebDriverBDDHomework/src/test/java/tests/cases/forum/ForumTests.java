package tests.cases.forum;

import org.junit.Test;
import pages.telerik.forum.ForumHomePage;
import pages.telerik.forum.ForumLoginPage;
import pages.telerik.forum.ForumTopicsPage;

public class ForumTests extends BaseTest {

    @Test
    public void openNewTopicWithValidTitleAndContent(){
        home.navigateToPage();
        home.assertLogInButtonVisible();
        home.clickLogInButton();

        login.enterEmail();
        login.enterPassword();
        login.clickSignInButton();

        topics.clickNewTopicButton();
        topics.enterTitle();
        topics.enterContent();
        topics.clickCreateTopicButton();

        topics.assertionForCreatedTopic();

        topics.logOut();
    }

    @Test
    public void tryToOpenNewTopicWithoutTitleAndContent(){
        home.navigateToPage();
        home.assertLogInButtonVisible();
        home.clickLogInButton();

        topics.clickNewTopicButton();
        topics.clickCreateTopicButton();

        topics.assertionForFailedCreationOfTopic();

        topics.logOut();
    }

}

