Meta:
@TopicCreation

Narrative:
As a user of Telerik Forum.
In order to make a new topic.
I want to open a new topic.


Scenario: Open a new topic with valid title and content.
Given Telerik academy forum homepage is opened
When Log in with valid credentials
And Create a new topic with valid title and content
Then The new topic should be created


