Meta:
@TopicCreation

Narrative:
As a user of Telerik Forum.
In order to make a new topic.
I want to open a new topic without title and content.

Scenario: Try to open a new topic without title and content.
Given Telerik academy forum homepage is opened
When Login
And Create a new topic without title and content
Then You should not be able to create a topic without title and content

