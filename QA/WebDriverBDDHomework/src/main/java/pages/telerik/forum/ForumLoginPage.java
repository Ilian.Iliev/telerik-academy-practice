package pages.telerik.forum;

import com.telerikacademy.testframework.pages.BasePage;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;

public class ForumLoginPage extends BasePage {

    public ForumLoginPage(WebDriver driver) {
        super(driver, "forum.loginPage");
    }

    public void enterEmail() {
        actions.typeValueInField("buggs.bunnies.telerik@gmail.com", "forum.loginPage.emailInput");
    }

    public void enterPassword() {
        actions.typeValueInField("Buggs-Bunn1es", "forum.loginPage.passwordInput");
    }

    public void clickSignInButton(){
        actions.clickElement("forum.loginPage.signInButton");
    }
}
