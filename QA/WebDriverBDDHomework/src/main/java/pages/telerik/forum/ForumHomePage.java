package pages.telerik.forum;

import com.telerikacademy.testframework.pages.BasePage;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;

public class ForumHomePage extends BasePage {

    public ForumHomePage(WebDriver driver) {
        super(driver, "forum.homePage");
    }

    public void assertLogInButtonVisible(){
        actions.isElementVisible("forum.homePage.loginButton");
    }

    public void clickLogInButton(){
        actions.clickElement("forum.homePage.loginButton");
    }

}
