package pages.telerik.forum;

import com.telerikacademy.testframework.Utils;
import com.telerikacademy.testframework.pages.BasePage;
import org.openqa.selenium.WebDriver;


public class ForumTopicsPage extends BasePage {

    public ForumTopicsPage(WebDriver driver) {
        super(driver, "forum.topicsPage");
    }

    public void clickNewTopicButton(){
        actions.waitForElementVisible("forum.topicsPage.newTopicButton");
        actions.clickElement("forum.topicsPage.newTopicButton");
    }

    public void enterTitle(){
        actions.typeValueInField("New Topic " + randomNumber(),"forum.topicsPage.titleField");
    }

    public void enterContent(){
        actions.typeValueInField("Text " + randomNumber(),"forum.topicsPage.contentField");
    }

    public void clickCreateTopicButton(){
        actions.waitForElementVisible("forum.topicsPage.createTopicButton");
        actions.clickElement("forum.topicsPage.createTopicButton");
    }

    public void logOut(){
        actions.clickElement("forum.topicsPage.userMenu");
        actions.waitForElementVisible("forum.topicsPage.userPreferences");
        actions.clickElement("forum.topicsPage.userPreferences");
        actions.waitForElementVisible("forum.topicsPage.logOutButton");
        actions.clickElement("forum.topicsPage.logOutButton");
    }

    public void assertionForCreatedTopic(){
        actions.assertElementPresent("forum.topicsPage.createdTopicForAssertion");
        Utils.LOG.info("New topic is created!");
    }

    public void assertionForFailedCreationOfTopic(){
        actions.assertElementPresent("forum.topicsPage.createTopicButton");
        Utils.LOG.info("The system did not allow the creation of a new topic as required!");
    }


    public long randomNumber(){
        long min = 10000000000L;
        long max = 20000000000L;
        long randomNumber = (long)Math.floor(Math.random()*(max-min+1)+min);
        return randomNumber;
    }
}




