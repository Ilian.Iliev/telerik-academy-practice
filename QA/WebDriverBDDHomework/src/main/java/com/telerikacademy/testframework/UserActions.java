package com.telerikacademy.testframework;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.concurrent.TimeUnit;

import static com.telerikacademy.testframework.Utils.getConfigPropertyByKey;

public class UserActions {
    public WebDriver getDriver() {
        return Utils.getWebDriver();
    }

    final WebDriver driver;
    private WebDriverWait wait;
    private Actions action;

    public UserActions() {
        this.driver = Utils.getWebDriver();
        wait = new WebDriverWait(driver, Integer.parseInt(getConfigPropertyByKey("config.defaultTimeoutSeconds")));
        action = new Actions(driver);
    }

    public static void loadBrowser(String baseUrlKey) {
        Utils.getWebDriver().get(getConfigPropertyByKey(baseUrlKey));
    }

    public static void quitDriver() {
        Utils.tearDownWebDriver();
    }

    public void clickElement(String key, Object... arguments) {
        String locator = getLocatorValueByKey(key, arguments);

        Utils.LOG.info("Clicking on element " + key);
        WebElement element = driver.findElement(By.xpath(locator));
        element.click();
    }

    public void hoverElement(String key, Object... arguments) {
        String locator = getLocatorValueByKey(key, arguments);
        WebElement element = driver.findElement(By.xpath(locator));
        Utils.LOG.info("Hover on element " + key);
        action.moveToElement(element).perform();
    }

    public void typeValueInField(String value, String field, Object... fieldArguments) {
        String locator = getLocatorValueByKey(field, fieldArguments);
        WebElement element = driver.findElement(By.xpath(locator));
        element.sendKeys(value);
    }

    public void switchToIFrame(String iframe) {
        // TODO: Implement the method
        WebElement iFrame = driver.findElement(By.xpath(Utils.getUIMappingByKey(iframe)));
        Utils.LOG.info("Switching to iFrame " + iframe);
        Utils.getWebDriver().switchTo().frame(iFrame);
    }

    //############# WAITS #########

    public void waitForElementVisible(String locatorKey, Object... arguments) {
        // TODO: Implement the method
        wait = new WebDriverWait(driver, Integer.parseInt(getConfigPropertyByKey("config.defaultTimeoutSeconds")));
        String locator = getLocatorValueByKey(locatorKey, arguments);
        wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath(locator))));
    }

    public void waitForElementVisibleUntilTimeout(String locatorKey, int seconds, Object... locatorArguments) {
        WebDriverWait customWait = new WebDriverWait(driver,seconds);
        String locator = getLocatorValueByKey(locatorKey, locatorArguments);

        customWait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(locator)));
        WebElement element = driver.findElement(By.xpath(locator));

        customWait.until(ExpectedConditions.visibilityOf(element));

        try {
            customWait.until(ExpectedConditions.visibilityOf(element));
        } catch (org.openqa.selenium.NoSuchElementException e) {
            Utils.LOG.info(locatorKey + " is not visible!");
        }
    }

    public void waitForElementPresent(String locatorKey, Object... arguments) {
        String locator = getLocatorValueByKey(locatorKey, arguments);
        try {
            wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(locator)));
        } catch (org.openqa.selenium.NoSuchElementException e) {
            Utils.LOG.info(locatorKey + " is not present!");
        }
    }

    public boolean isElementPresent(String locatorKey, Object... arguments) {
        String locator = getLocatorValueByKey(locatorKey, arguments);
        if(wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(locator)))==null) {
            return false;
        }
        else {
            Utils.LOG.info(locatorKey + " is visible!");
            return true;
        }
    }

    public boolean isElementVisible(String locatorKey, Object... arguments) {
        String locator = getLocatorValueByKey(locatorKey, arguments);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(locator)));
        if(!driver.findElement(By.xpath(locator)).isDisplayed()) {
            return false;
        }
        Utils.LOG.info(locatorKey + " is visible!");
        return true;
    }

    public void waitFor(long timeOutMilliseconds) {
        try {
            Thread.sleep(timeOutMilliseconds);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    //############# ASSERTS #########

    public void assertElementPresent(String locator) {
        Assert.assertNotNull(driver.findElement(By.xpath(Utils.getUIMappingByKey(locator))));
    }

    public void assertElementAttribute(String locator, String attributeName, String attributeValue) {
        WebElement element = driver.findElement(By.xpath(Utils.getUIMappingByKey(locator)));
        String valueOfAttribute = element.getAttribute(attributeName);
        Assert.assertEquals("Attribute value is not equal!",valueOfAttribute, attributeValue);
    }

    public void assertNavigatedUrl(String urlKey) {
        String currentUrl = driver.getCurrentUrl();
        Assert.assertTrue("Navigated URL is different!", currentUrl.contains(getConfigPropertyByKey(urlKey)));

    }

    public void pressKey(Keys key) {
        action.keyDown(key).keyUp(key);
    }

    private String getLocatorValueByKey(String locator, Object[] arguments) {
        return String.format(Utils.getUIMappingByKey(locator), arguments);
    }
}
