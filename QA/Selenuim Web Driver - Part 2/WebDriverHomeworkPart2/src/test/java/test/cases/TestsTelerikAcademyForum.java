package test.cases;

import pages.ForumHomePage_Factory;
import pages.ForumTopicsPage_Factory;
import pages.LoginPage_Factory;
import org.junit.*;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;


public class TestsTelerikAcademyForum extends BaseTest {

    private final String URL = "https://stage-forum.telerikacademy.com/";



    @Test
    public void openNewTopicWithTitleAndContent(){
        ForumHomePage_Factory homePage = new ForumHomePage_Factory(webdriver);
        LoginPage_Factory loginPage = new LoginPage_Factory(webdriver);
        ForumTopicsPage_Factory topicsPage = new ForumTopicsPage_Factory(webdriver);

        //Arrange
        webdriver.get(URL);

        //Act
        homePage.clickLoginButton();

        loginPage.enterEmail();
        loginPage.enterPassword();
        loginPage.clickSignInButton();

        topicsPage.clickNewTopicButton();
        topicsPage.enterTitle();
        topicsPage.enterContent();
        topicsPage.clickCreateTopicButton();

        //Assert
        WebElement createdTopicForAssertion = webdriver.findElement(By.xpath("//button[@aria-label='Reply']"));
        Assert.assertTrue("Created topic was not displayed.",createdTopicForAssertion.isDisplayed());

        topicsPage.logOut();

    }

    @Test
    public void tryToOpenNewTopicWithoutTitle(){
        ForumHomePage_Factory homePage = new ForumHomePage_Factory(webdriver);
        LoginPage_Factory loginPage = new LoginPage_Factory(webdriver);
        ForumTopicsPage_Factory topicsPage = new ForumTopicsPage_Factory(webdriver);

        //Arrange
        webdriver.get(URL);

        //Act
        homePage.clickLoginButton();

        loginPage.enterEmail();
        loginPage.enterPassword();
        loginPage.clickSignInButton();

        topicsPage.clickNewTopicButton();
        topicsPage.clickCreateTopicButton();


        //Assert
        Assert.assertTrue("Created topic is displayed.",topicsPage.createTopicButton.isDisplayed());

        topicsPage.logOut();
    }
}
