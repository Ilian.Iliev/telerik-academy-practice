package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;


public class LoginPage_Factory {

    private WebDriver driver;

    public LoginPage_Factory(WebDriver webDriver){
        driver = webDriver;
        PageFactory.initElements(webDriver, this);
    }

    @FindBy(id="Email")
    public WebElement emailField;

    @FindBy(id="Password")
    public WebElement passwordField;

    @FindBy(id="next")
    public WebElement signInButton;

    @FindBy(id="TelerikAcademyAD")
    public WebElement signInWithTelerikAcademy;

    @FindBy(xpath="//a[contains(text(),'Sign Up')]")
    public WebElement SignUpButton;

    public void enterEmail(){
        emailField.sendKeys("buggs.bunnies.telerik@gmail.com");
    }

    public void enterPassword(){
        passwordField.sendKeys("Buggs-Bunn1es");
    }

    public void clickSignInButton(){
        signInButton.click();
    }
}
