package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class ForumTopicsPage_Factory {

    private WebDriver driver;
    private WebDriverWait wait;

    public ForumTopicsPage_Factory(WebDriver webDriver){
        driver = webDriver;
        PageFactory.initElements(webDriver, this);
        wait = new WebDriverWait(webDriver,5);
    }

    @FindBy(id="create-topic")
    public WebElement newTopicButton;

    @FindBy(xpath="//textarea[@aria-label='Type here. Use Markdown, BBCode, or HTML to format. Drag or paste images.']")
    public WebElement contentField;

    @FindBy(id="reply-title")
    public WebElement titleField;

    @FindBy(xpath="//button[@aria-label='Create Topic']")
    public WebElement createTopicButton;

    @FindBy(id="current-user")
    public WebElement userMenu;

    @FindBy(xpath = "//button[@aria-controls='quick-access-profile']")
    public WebElement preferences;

    @FindBy(xpath = "//span[text()='Log Out']")
    public WebElement logoutButton;


    public void clickNewTopicButton(){
        wait.until(ExpectedConditions.visibilityOf(newTopicButton));
        newTopicButton.click();
    }

    public void enterTitle(){
        wait.until(ExpectedConditions.visibilityOf(titleField));
        titleField.sendKeys("Title " + randomNumber());
    }

    public void enterContent(){
        contentField.sendKeys("Content " + randomNumber());
    }

    public void clickCreateTopicButton(){
        wait.until(ExpectedConditions.visibilityOf(createTopicButton));
        createTopicButton.click();
    }

    public void logOut(){
        userMenu.click();
        wait.until(ExpectedConditions.visibilityOf(preferences));
        preferences.click();
        wait.until(ExpectedConditions.visibilityOf(logoutButton));
        logoutButton.click();
    }

    public long randomNumber(){
        long min = 10000000000L;
        long max = 20000000000L;
        long randomNumber = (long)Math.floor(Math.random()*(max-min+1)+min);
        return randomNumber;
    }



}
