package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class ForumHomePage_Factory {

    private WebDriver driver;

    public ForumHomePage_Factory(WebDriver webDriver){
        driver = webDriver;
        PageFactory.initElements(webDriver, this);
    }

    @FindBy(xpath="//button[@class='widget-button btn btn-primary btn-small login-button btn-icon-text']")
    public WebElement loginButton;

    public void clickLoginButton(){
        loginButton.click();
    }
}
