**Test case:**

1.Try to log in with wrong password

**Narrative:**	

1.In order to log in in the forum. 
2.As an user of the forum. 
3.I want to try to login with wrong password

**Steps to reproduce:**

1. Click "Log in" 
2. Write a valid username in the "username" field. 
3. Write an invalid password. 
4. Click "Log in"


**Expected result:**

Warning message "Invalid usarname or password"

**Priority:**

Prio 1

**Technique:** 

Error guessing

---------------------------------------------------------
**Test case:**

2.Open a new topic with title and content.

**Narrative:**

1.In order to make a new topic. 
2.As an user of the forum. 
3.I want to open a new topic viewble by all users.	

**Steps to reproduce:**

1. Log in with your account. 
2. Go to main page and click "New Topic"
3. In field "Title" write a short title of the topic. 
4. In textbox write a short text.
5. Click "Publish" button

**Expected result:**

New topic, visible by all users is created. 

**Priority:**

Prio 1

**Technique:**

Use case testing

---------------------------------------------------------
**Test case:**

3.Open a new topic with category and tag.

**Narrative:**

1.In order to make a new topic. 
2.As an user of the forum. 
3.I want to open a new topic viewble by all users.	

**Steps to reproduce:**

1. Log in with your account. 
2. Go to main page and click "New Topic"
3. In field "Title" write a short title of the topic. 
4. In textbox write a short text.
5. Select a category.
6. select a tag.
7. Click "Publish" button

**Expected result:**

New topic, visible by all users, placed in the right category and with visible tag is created. 

**Priority:**

Prio 2

**Technique:** 

Pairwise testing

---------------------------------------------------------
**Test case:**

4.Try to create a new topic while you are not logged in to your account.

**Narrative:**	

1.In order to create a new topic. 
2.As a guest of the forum. 
3.I want to try to create a new topic. 

**Steps to reproduce:**

1. Open the homepage as a guest
2. Try to find "+ New Topic" button

**Expected result:**

"+ New Topic" button should not be available before log in. 

**Priority:**

Prio 3

**Technique:**

Error guessing

---------------------------------------------------------
**Test case:**

5.Open a new topic without title.

**Narrative:**

1.In order to make a new topic. 
2.As an user of the forum. 
3.I want to open a new topic viewble by all users.	

**Steps to reproduce:**

1. Log in with your account. 
2. Go to main page and click "New Topic"
3. Keep the "Title" field empty.
4. Click "Publish" button

**Expected result:**

Warning message "Title is required"

**Priority:**

Prio 2

**Technique:**

Boundary value analysis

---------------------------------------------------------
**Test case:**

6.Open a new topic without content.

**Narrative:**

1.In order to make a new topic. 
2.As an user of the forum. 
3.I want to open a new topic viewble by all users.	

**Steps to reproduce:**

1. Log in with your account. 
2. Go to main page and click "New Topic"
3. Keep the "content" field empty.
4. Click "Publish" button

**Expected result:**

Warning message "Post can't be empty"

**Priority:**

 Prio 2

**Technique:**

Boundary value analysis

---------------------------------------------------------
**Test case:**

7.Open a new topic with title but without content.

**Narrative:**

1.In order to make a new topic. 
2.As an user of the forum. 
3.I want to open a new topic viewble by all users.	

**Steps to reproduce:**

1. Log in with your account. 
2. Go to main page and click "New Topic"
3. Write a short title.
3. Keep the "content" field empty.
4. Click "Publish" button

**Expected result:**

Warning message "Post can't be empty"

**Priority:** 

Prio 2

**Technique:**

Pairwise testing

---------------------------------------------------------
**Test case:**

8.Open a new topic with content but without title.

**Narrative:**	

1.In order to make a new topic. 
2.As an user of the forum. 
3.I want to open a new topic viewble by all users.	

**Steps to reproduce:**

1. Log in with your account. 
2. Go to main page and click "New Topic"
3. Write a short content.
4. Keep the "Title" field empty.
5. Click "Publish" button

**Expected result:**

Warning message "Title is required"

**Priority:**

Prio 2

**Technique:**

Pairwise testing

---------------------------------------------------------
**Test case:**

9.Open a new topic with content less than 10 symbols and title less than 5 symbols.

**Narrative:**

1.In order to make a new topic. 
2.As an user of the forum. 
3.I want to open a new topic viewble by all users.	

**Steps to reproduce:**

1. Log in with your account. 
2. Go to main page and click "New Topic"
3. Write a content with less than 10 symbols.
4. Write a title with less than 5 symbols.
5. Click "Publish" button

**Expected result:**

Warning messages: "Title must be at least 5 characters" , "Post must be at least 10 characters"

**Priority:** 

Prio 3

**Technique:**

Boundary value analysis

---------------------------------------------------------

**Test case:**

10.Open a new topic with the minimum required symbols for title and content. 

**Narrative:**

1.In order to make a new topic. 
2.As an user of the forum. 
3.I want to open a new topic viewble by all users.	

**Steps to reproduce:**

1. Log in with your account. 
2. Go to main page and click "New Topic"
3. Write a content with 10 symbols.
4. Write a title with 5 symbols.
5. Click "Publish" button

**Expected result:**

New topic should be created.

**Priority:**

Prio 2

**Technique:**
Boundary value analysis

---------------------------------------------------------
**Test case:**

11.Write, create, edit and delete a topic. 

**Narrative:**

1.In order to write, create, edit and delete a topic. 
2.As an user of the forum. 
3.I want to check all transitions of the topic.

**Steps to reproduce:**

1. Log in with your account. 
2. Go to main page and click "+New Topic"
3. In field "Title" write a short title of the topic. 
4. In textbox write a short text.
5. Click "Publish" button
6. Go to main page and find the created topic.
7. Click "Edit" button
8. Change the content.
9. Click "Save" button.
10. Find the topic. 
11. Click "Delete" button.

**Expected result:**

New topic should be created successfully, edited and finally deleted. 

**Priority:**

Prio 1

**Technique:**

State transition testing

---------------------------------------------------------
**Test case:**

12.Write a comment. 

**Narrative:**

1.In order to write a new comment. 
2.As an user of the forum. 
3.I want to write a new comment at created topic.

**Steps to reproduce:**

1. Log in with your account. 
2. Go to main page and open a created topic.
3. Click "Comment" button. 
4. In textbox write a short text.
5. Click "Publish" button.

**Expected result:**

New comment, visible by all users is created. 

**Priority:**

Prio 1

**Technique:**

Use case testing

---------------------------------------------------------
**Test case:**

13.Write a comment with blockquote and attached image. 

**Narrative:**

1.In order to write a new comment. 
2.As an user of the forum. 
3.I want to write a new comment at created topic.

**Steps to reproduce:**

1. Log in with your account. 
2. Go to main page and open a created topic.
3. Click "Comment" button. 
4. In textbox write a short text including blockquote.
5. Attach an image.
5. Click "Publish" button.

**Expected result:**

New comment with blockquote and attached image, visible by all users is created. 

**Priority:**

Prio 2

**Technique:**

Pairwise testing

---------------------------------------------------------
**Test case:**

14.Write a comment from different account and check if the author's account has notification for your comment.

**Prerequisites:**

2 valid accounts needed. 

**Narrative:**

1.In order to write a new comment and check the notifications. 
2.As an user of the forum. 
3.I want to write a new comment at created topic and check if the author recieves a notification.

**Steps to reproduce:**

1. Log in with author's account. 
2. Go to home page and click "+New Topic"
3. In field "Title" write a short title of the topic. 
4. In textbox write a short text.
5. Click "Publish" button
6. Log out.
7. Log in with second account. 
8. Go to home page and find the created topic.
9. Click "Comment" button. 
10. In textbox write a short text.
11. Click "Publish" button.
12. Log out.
13. Log in with author's account.
14. Check if you have notification for the new comment.

**Expected result:**

The author of the topic should recieve a notification for the new comment. 

**Priority:**

 Prio 3

**Technique:**

Error guessing 



















