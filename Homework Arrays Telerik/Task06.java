package com.telerikacademy.core;

import java.util.ArrayList;
import java.util.Scanner;

public class Task06 {
    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        String[] numbers = sc.nextLine().split(" ");


        ArrayList<Integer> arr = new ArrayList();
        ArrayList<Integer> arr2 = new ArrayList();

        ArrayList<Integer> result = new ArrayList();

        int biggerInput = Integer.parseInt(numbers[0]) >= Integer.parseInt(numbers[1]) ? Integer.parseInt(numbers[0]) : Integer.parseInt(numbers[1]);

        String[] numbersOfFirstArray = sc.nextLine().split(" ");
        String[] numbersOfSecondArray = sc.nextLine().split(" ");

        for (int i = 0; i < Integer.parseInt(numbers[0]); i++){
            if (i >= numbersOfFirstArray.length){
                arr.add(Integer.parseInt(numbers[i]));
            }
            else{
                arr.add(Integer.parseInt(numbersOfFirstArray[i]));
            }
        }

        for (int i = 0; i < Integer.parseInt(numbers[1]); i++){
            if (i >= numbersOfSecondArray.length){
                arr.add(Integer.parseInt(numbers[i]));
            }
            else{
                arr.add(Integer.parseInt(numbersOfSecondArray[i]));
            }
        }
        int rest = 0;
        for (int i = 0; i < biggerInput; i++){

            int numberOfFirstArray = i >= numbersOfFirstArray.length ? 0 : Integer.parseInt(numbersOfFirstArray[i]);
            int numberOfSecondArray = i >= numbersOfSecondArray.length ? 0 : Integer.parseInt(numbersOfSecondArray[i]);
            int sum = numberOfFirstArray + numberOfSecondArray + rest;
            result.add(sum%10);
            if (sum > 9){
                rest = 1;
            }
            else {
                rest = 0;
            }
        }

        for (Integer num : result) {
            System.out.print(num + " ");
        }

    }
}
