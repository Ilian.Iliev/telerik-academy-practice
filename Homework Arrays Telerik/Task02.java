package com.telerikacademy.core;

import java.util.Scanner;

public class Task02 {
    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        int n = Integer.parseInt(sc.nextLine());

        for (int i = 0; i < n; i++) {
            String[] arr = sc.nextLine().split(" ");
            boolean isSymetric = true;
            for (int j = 0; j < arr.length / 2; j++) {
                if (!arr[j].equals(arr[arr.length-1-j])) {
                    isSymetric = false;
                    System.out.println("No");
                    break;
                }
            }
            if (isSymetric) {
                System.out.println("Yes");
            }
        }
    }
}
