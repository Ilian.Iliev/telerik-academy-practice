package com.telerikacademy.core;

import java.util.Scanner;

public class Task03 {
    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        String[] arr = sc.nextLine().split(" ");

        for (int i = 0 ; i < arr.length; i ++){
            int a = Integer.parseInt(arr[i]);
            if(a % 3 == 0){
                System.out.print(arr[i] + " ");
            }
        }
        System.out.println();

        for (int i = 0 ; i < arr.length; i ++){
            int a = Integer.parseInt(arr[i]);
            if(a % 3 == 1){
                System.out.print(arr[i] + " ");
            }
        }
        System.out.println();

        for (int i = 0 ; i < arr.length; i ++){
            int a = Integer.parseInt(arr[i]);
            if(a % 3 == 2){
                System.out.print(arr[i] + " ");
            }
        }
    }
}
