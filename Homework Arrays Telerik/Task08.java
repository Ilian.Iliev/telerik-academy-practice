package com.telerikacademy.core;

import java.util.Arrays;
import java.util.Scanner;

public class Task08 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        int num = sc.nextInt();

        int[][] arr = new int[num][num];

        int x = 0;
        int y = 0;
        int directionX = 0;
        int directionY = 1;
        int sum = 1;
        boolean b1 = true;
        boolean b2 = true;
        for(int i = 0;;i++) {

            arr[x][y] = sum;
            if (sum == num * num) {
                break;
            }

            //horizontal
            if(i != 0 && directionX == 0) {
                if((x + directionX < arr.length && x + directionX >= 0) && (y + directionY < arr.length && y + directionY >= 0)){
                    if(arr[x + directionX][y + directionY] != 0) {
                        directionY = 0;
                        if(b1)
                        {
                            directionX = 1;
                            b1 = false;
                        }
                        else
                        {
                            directionX = -1;
                            b1 = true;
                        }
                    }
                }
                else {

                    if(y == arr[x].length - 1) {
                        directionX = 1;
                        directionY = 0;
                    }
                    if(y == 0) {
                        directionX = -1;
                        directionY = 0;
                    }
                }
            }

            //vertical
            if(i != 0 && directionY == 0) {
                if((x + directionX < arr.length && x + directionX >= 0) && (y + directionY < arr.length && y + directionY >= 0) ){
                    if(arr[x + directionX][y + directionY] != 0) {
                        if(b2)
                        {
                            directionY = 1;
                            b2 = false;
                        }
                        else
                        {
                            directionY = -1;
                            b2 = true;
                        }
                        directionX = 0;
                    }
                }
                else {
                    if(x == arr.length - 1) {
                        directionX = 0;
                        directionY = -1;
                    }
                    if(x == 0) {
                        directionX = 0;
                        directionY = 1;
                    }
                }
            }

            x+=directionX;
            y+=directionY;
            sum++;
        }

        for (int i = 0; i < arr.length; i++){
            for (int j = 0; j < arr[i].length; j++){
                if(j == arr[i].length-1) {
                    System.out.print(arr[i][j]);
                }
                else {
                    System.out.print(arr[i][j] + " ");
                }
            }
            System.out.println();
        }
        sc.close();
    }
}
