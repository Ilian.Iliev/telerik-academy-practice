package com.telerikacademy.core;

import java.util.Scanner;

public class Task05 {
    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();

        int[] arr = new int[n];

        for (int i = 0; i < arr.length; i++) {
            arr[i] = sc.nextInt();
        }
        int sequence = 0;
        int cnt = 1;

            for (int y = 1; y < arr.length; y++) {
                if (arr[y-1] == arr[y]) {
                    cnt++;
                }
                if (cnt > sequence){
                    sequence = cnt;
                }
                if (arr[y-1] != arr[y]) {
                    cnt=1;
                }

            }

        System.out.println(sequence);
    }
}
