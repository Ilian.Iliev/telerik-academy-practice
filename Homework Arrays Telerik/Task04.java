package com.telerikacademy.core;

import java.util.Scanner;

public class Task04 {
    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();

        int[][] arr = new int[n][n];
        arr[0][0] = 1;

        for (int i = 0; i < arr.length; i++){
            for (int y = 0; y < arr[i].length; y++){
                if(i+y == 0) {
                    continue;
                }
                else if (y == 0){
                    arr[i][y] = (arr[i-1][y])*2;
                }
                else {
                    arr[i][y] = (arr[i][y - 1]) * 2;
                }
            }
        }

        long sum = 0L;

        for (int i = 0;i < arr.length; i++){
            int y = 0+i;
            while (y < arr[i].length){
                sum = sum + arr[i][y];
                y++;
            }
        }
        System.out.println(sum);
    }
}
