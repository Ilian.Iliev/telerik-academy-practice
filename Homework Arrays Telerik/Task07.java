package com.telerikacademy.core;

import java.util.Scanner;

public class Task07 {
    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        String[] numbers1 = sc.nextLine().split(" ");

        long[][] arr = new long[Integer.parseInt(numbers1[0])][Integer.parseInt(numbers1[1])];
        arr[0][0] = 1;

        for (int i = 0; i < arr.length; i++){
            for (int y = 0; y < arr[i].length; y++){
                if(i+y == 0) {
                    continue;
                }
                else if (y == 0){
                    arr[i][y] = (arr[i-1][y])*2;
                }
                else {
                    arr[i][y] = (arr[i][y - 1]) * 2;
                }
            }
        }

        for (int i = 0 ; i < arr.length ; i++) {
            for (int j = 0; j < arr[i].length; j++) {
                System.out.print(arr[i][j] + " ");
            }
            System.out.println();
        }
        long sum = 0;
        int directionX = 1;
        int directionY = 1;
        int x = 0;
        int y = 0;


        for(int i = 0;;i++){

            if ((x == 0 && y == 0) && (i != 0)) {
                sum += arr[x][y];
                break;
            }
            if (x == 0 && y == arr[x].length-1) {
                sum += arr[x][y];
                break;
            }
            if (x == arr.length-1 && y == arr[x].length-1) {
                sum += arr[x][y];
                break;
            }
            if (x == arr.length-1 && y == 0) {
                sum += arr[x][y];
                break;
            }
            sum += arr[x][y];

            if (x == arr.length-1) {
                directionX = -1;
            }
            if (x == 0) {
                directionX = 1;
            }
            if (y == arr[x].length-1){
                directionY = -1;
            }
            if (y == 0){
                directionY = 1;
            }
            x += directionX;
            y += directionY;
        }

        System.out.println(sum);
        sc.close();
    }
}