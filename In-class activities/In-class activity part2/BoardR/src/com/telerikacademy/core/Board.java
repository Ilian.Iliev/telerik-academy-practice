package com.telerikacademy.core;

import java.util.ArrayList;
import java.util.List;

public  class Board {

    private static List<BoardItem>items=new ArrayList<>();
    private Board(){

    }

    public static void addItem(BoardItem item)
    {
        if (items.contains(item)) {
            throw new IllegalArgumentException("Item already in the list");
        }
        Board.items.add(item);
    }

    public static int totalItems(){
        int count = Board.items.size();
        return count;
    }



}
