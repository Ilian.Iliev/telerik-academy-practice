package com.telerikacademy.core;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class EventLog {
    private final String description;
    private final LocalDateTime createDate;
    DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss");


    public EventLog (String description){
        if(description == null){
            throw new IllegalArgumentException("Description cannot be null");
        }
        this.description = description;

        this.createDate = LocalDateTime.now();
    }

    public String getDescription() {
        return description;
    }

    public String viewInfo() {
        return String.format("[ %s ] , '%s'", dtf.format(createDate), getDescription());
    }
}
