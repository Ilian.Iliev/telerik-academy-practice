package com.telerikacademy.core;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class BoardItem {

    private final Status initialStatus = Status.OPEN;
    private final Status finalStatus = Status.VERIFIED;


    private String title;
    private LocalDate dueDate;
    private Status status;
    private List<EventLog> logs = new ArrayList<>();

    public BoardItem(String title, LocalDate dueDate) {
        this(title, dueDate, Status.OPEN);
    }


    public BoardItem(String title, LocalDate dueDate, Status status) {
        validateTitle(title);
        this.title = title;
        validateDueDate(dueDate);
        this.dueDate = dueDate;
        setStatus(status);
        logs.add(new EventLog("Item created: " + viewInfo()));
    }

    private void validateDueDate(LocalDate dueDate)
    {
        if (dueDate.isBefore(LocalDate.now())) {
            throw new IllegalArgumentException("DueDate can't be in the past");
        }
    }

    public void setDueDate(LocalDate dueDate) {
        validateDueDate(dueDate);
        logs.add(new EventLog("DueDate changed from " + this.dueDate + "  to " + dueDate));
        this.dueDate = dueDate;
    }

    public LocalDate getDueDate() {
        return dueDate;
    }

    private void validateTitle(String title)
    {
        if (title == null) {
            throw new IllegalArgumentException("Please provide a non-empty title");
        }

        if (title.length() < 5 || title.length() > 30) {
            throw new IllegalArgumentException("Please provide a title with length between 5 and 30 chars");
        }
    }

    public void setTitle(String title) {
        validateTitle(title);
        logs.add(new EventLog("Title changed from " + this.title + "  to " + title));
        this.title = title;

    }

    public String getTitle() {
        return title;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public Status getStatus() {
        return status;
    }

    public void revertStatus() {
        if (this.status != initialStatus) {
            logs.add(new EventLog("Status changed from " + this.status + " to " + Status.values()[status.ordinal() - 1]));
            status = Status.values()[status.ordinal() - 1];
            return;
        }
        logs.add(new EventLog("Can't revert, already at " + this.status ));
    }

    public void advanceStatus() {
        if (this.status != finalStatus) {
            logs.add(new EventLog("Status changed from " + this.status + " to " + Status.values()[status.ordinal() + 1]));
            status = Status.values()[status.ordinal() + 1];
            return;
        }
        logs.add(new EventLog("Can't advance, already at " + this.status ));
    }


    public String viewInfo() {
        return String.format("'%s', [%s | %s]", title, status.toString(), dueDate.toString());
    }

    public void displayHistory() {
        StringBuilder history = new StringBuilder();
        for ( EventLog log : logs) {
            history.append(log.viewInfo()).append("\n");
        }
        System.out.println(history.toString());
    }


}
