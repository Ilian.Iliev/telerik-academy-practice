package com.telerikacademy.core;
import java.time.LocalDate;

public class Main {

    public static void main(String[] args) {

        BoardItem item1 = new BoardItem("Implement login/logout", LocalDate.now().plusDays(3));
        BoardItem item2 = new BoardItem("Secure admin endpoints", LocalDate.now().plusDays(5));

        Board.addItem(item1); // add item1
        Board.addItem(item2); // add item2
        Board.addItem(item1);
        Board.addItem(item2);
        Board.addItem(item1);


        System.out.println(Board.totalItems()); // count: 2


    }
}
