package com.telerikacademy.core;

public enum ItemStatus {

    OPEN,
    TODO,
    INPROGRESS,
    DONE,
    VERIFIED;

    private static ItemStatus[] allStatuses = ItemStatus.values();

    public ItemStatus getNext() {
        return allStatuses[(this.ordinal() + 1) % allStatuses.length];
    }
    public ItemStatus getPrevious() {
        return allStatuses[(this.ordinal() - 1 + allStatuses.length) % allStatuses.length];
    }

}
