package com.telerikacademy.core;

import java.time.LocalDate;

public class BoardItem {

    public String title;
    public LocalDate dueDate;
    public ItemStatus status;

    public BoardItem(String title, LocalDate dueDate){
        setTitle(title);
        setDueDate(dueDate);
        setStatus(ItemStatus.OPEN);
    }


    public BoardItem(String title, LocalDate dueDate, ItemStatus status) {
        boolean isTitleValid = !title.isEmpty() && title.length() >= 5 && title.length() <= 30;
        if (isTitleValid && isValidDate(dueDate)) {
            setTitle(title);
            setDueDate(dueDate);
        } else {
            throw new IllegalArgumentException("invalid type");
        }
        setStatus(status);
    }

    public void setDueDate(LocalDate dueDate) {
        this.dueDate = dueDate;
    }

    public LocalDate getDueDate() {
        return dueDate;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTitle() {
        return title;
    }

    public void setStatus(ItemStatus status) {
        this.status = status;
    }

    public ItemStatus getStatus() {
        return status;
    }


    public boolean isValidDate(LocalDate futureDate) {
        return LocalDate.now().isBefore(futureDate);
    }


    public ItemStatus advanceStatus() {
        if (status != ItemStatus.VERIFIED){
            status = status.getNext();
        }
        return status;
    }

    public ItemStatus revertStatus() {
        if (status != ItemStatus.OPEN) {
            status = status.getPrevious();
        }
        return status;
    }

    public String viewInfo() {
        return String.format("'%s', [%s | %s]", title, status.toString(), dueDate.toString());
    }


}
