

import com.telerikacademy.StackImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Arrays;

public class StackTests {
    
    private StackImpl<Integer> testStack;
    
    @BeforeEach
    public void before() {
        this.testStack = new StackImpl<>();
    }

    @Test
    public void constructor_Should_CreateEmptyStack(){
        Assertions.assertEquals(0, testStack.size());
    }

    @Test
    public void constructor_Should_CreateStackWithElements(){
        this.testStack = new StackImpl<>(Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8, 9));
        Assertions.assertEquals(9, testStack.size());
    }

    @Test
    public void push_Should_AddElementToTheStack() {
        testStack.push(1);
        testStack.push(2);
        Assertions.assertEquals(2, testStack.size());
    }

    @Test
    public void pop_Should_ThrowException_When_StackSizeIsNull(){
        Assertions.assertThrows(IllegalArgumentException.class, () -> testStack.pop(),"Stack is empty");
    }

    @Test
    public void pop_ShouldReturnDeletedElement() {
        testStack = new StackImpl<>(Arrays.asList(22,33,44));
        Assertions.assertEquals(44, testStack.pop());
    }

    @Test
    public void pop_ShouldUpdateSize() {
        testStack = new StackImpl<>(Arrays.asList(1,2));
        testStack.pop();
        Assertions.assertEquals(1, testStack.size());
    }

    @Test
    public void peek_Should_ThrowException_When_StackSizeIsNull(){
        Assertions.assertThrows(IllegalArgumentException.class, () -> testStack.peek(),"Stack is empty");
    }

    @Test
    public void peek_Should_ReturnTheTopElementOfTheStack() {
        testStack.push(1);
        testStack.push(3);
        Assertions.assertEquals(3, testStack.peek());
    }

    @Test
    public void size_Should_CheckStackSize() {
        testStack.push(3);
        testStack.push(4);
        testStack.pop();
        Assertions.assertEquals(1, testStack.size());
    }

    @Test
    public void isEmpty_ShouldReturnTrue_WhenStackIsEmpty(){
        Assertions.assertTrue(testStack.isEmpty());
    }

}
