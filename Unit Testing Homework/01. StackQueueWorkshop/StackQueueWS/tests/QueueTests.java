

import com.telerikacademy.Queue;
import com.telerikacademy.QueueImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Arrays;

public class QueueTests {
    
    private Queue<Integer> testQueue;
    
    @BeforeEach
    public void before() {
        testQueue = new QueueImpl<Integer>();
    }

    @Test
    public void constructor_Should_CreateEmptyQueue(){
        Assertions.assertEquals(0,testQueue.size());
    }

    @Test
    public void constructor_Should_CreateQueueWithElements(){
        testQueue = new QueueImpl<>(Arrays.asList(1,2,3,4,5));
        Assertions.assertEquals(5,testQueue.size());
    }

    @Test
    public void offer_Should_AddElementToTheQueue() {
        testQueue.offer(2);
        testQueue.offer(3);
        Assertions.assertEquals(2, testQueue.size());
    }

    @Test
    public void poll_Should_ThrowException_When_Empty() {
        Assertions.assertThrows(IllegalArgumentException.class, () -> testQueue.poll());
    }

    @Test
    public void poll_Should_ReturnFirstElementOfTheQueue(){
        testQueue.offer(9);
        testQueue.offer(8);
        Assertions.assertEquals(9, testQueue.poll());
    }

    @Test
    public void poll_Should_ReduceTheSizeOfTheQueue(){
        testQueue.offer(9);
        testQueue.offer(8);
        testQueue.poll();
        Assertions.assertEquals(1, testQueue.size());
    }

    @Test
    public void peek_Should_ThrowException_When_QueueSizeIsNull(){
        Assertions.assertThrows(IllegalArgumentException.class, () -> testQueue.peek(),"Queue is empty");
    }

    @Test
    public void peek_Should_ReturnTheFirstAddedElementOfTheQueue() {
        testQueue.offer(1);
        testQueue.offer(3);
        Assertions.assertEquals(1, testQueue.peek());
    }

    @Test
    public void size_ShouldReturnNumberOfElements(){
        testQueue = new QueueImpl<>(Arrays.asList(22,33,44));
        Assertions.assertEquals(3, testQueue.size());
    }

    @Test
    public void isEmpty_ShouldReturnTrue_WhenQueueIsEmpty(){
        Assertions.assertTrue(testQueue.isEmpty());
    }

    @Test
    public void isEmpty_ShouldReturnFalse_WhenQueueIsFilled(){
        testQueue = new QueueImpl<>(Arrays.asList(22,33,44));
        Assertions.assertFalse(testQueue.isEmpty());
    }


}
